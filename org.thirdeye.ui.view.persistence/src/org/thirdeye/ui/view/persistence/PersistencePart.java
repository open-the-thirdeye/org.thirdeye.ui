/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.ui.view.persistence;

import javax.inject.Inject;
import javax.annotation.PostConstruct;

import org.apache.commons.lang.ArrayUtils;
import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.thirdeye.core.integrator.Integrator;
import org.thirdeye.core.mesh.Accessor;
import org.thirdeye.core.mesh.Edge;
import org.thirdeye.core.mesh.Mesh;
import org.thirdeye.core.mesh.Node;
import org.thirdeye.core.mesh.Variable;
import org.thirdeye.core.persistence.Persistence;
import org.thirdeye.core.persistence.PersistenceFactory;
import org.thirdeye.core.persistence.SimulationRun;
import org.thirdeye.core.persistence.impl.PersistencePackageImpl;
import org.thirdeye.core.sequence.Sequence;
import org.thirdeye.core.sequence.SimpleSequence;

public class PersistencePart {
	
	private TreeViewer treeViewer;

	@Inject
	private ESelectionService selectionService;
	
	class ViewContentProvider implements ITreeContentProvider {
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			PersistencePackageImpl.init();
			PersistenceFactory factory = PersistenceFactory.eINSTANCE;
			Persistence persistence = factory.createPersistence();
			
			@SuppressWarnings("unchecked")
			EList<Mesh> listM = (EList<Mesh>) persistence.getBackend().getStoredMeshes();
			//System.out.println("Meshes loaded: " + listM.toString());
			@SuppressWarnings("unchecked")
			EList<EObject> listR = (EList<EObject>) persistence.getBackend().getStoredRunners();
			//System.out.println("Runners loaded: " + listR.toString());
			
			listR.addAll(listM);
			return listR.toArray();
		}
		
		@Override
		public Object[] getChildren(Object parentElement) {
			if( parentElement instanceof SimulationRun ) {
				Sequence seq = ((SimulationRun) parentElement).getSequence();
				return new Object[] { seq };
			} else if( parentElement instanceof Sequence ) {
				Mesh mesh = ((Sequence) parentElement).getMesh();
				Integrator integ = ((SimpleSequence) parentElement).getIntegrator();
				return new Object[] { mesh,integ };
			} else if( parentElement instanceof SimulationRun ) {
				Sequence seq = ((SimulationRun) parentElement).getSequence();
				return new Object[] { seq };
			} else if( parentElement instanceof Mesh) {
				Object[] temp1 = ArrayUtils.addAll(((Mesh) parentElement).getModelNodes().toArray(), ((Mesh) parentElement).getConnectionNodes().toArray() );
				Object[] temp2 = ArrayUtils.addAll(temp1, ((Mesh) parentElement).getProviderNodes().toArray());
				Object[] temp3 = ArrayUtils.addAll(temp2, ((Mesh) parentElement).getFromModelEdges().toArray() );
				Object[] temp4 = ArrayUtils.addAll(temp3, ((Mesh) parentElement).getFromConnectionEdges().toArray() );
				Object[] temp5 = ArrayUtils.addAll(temp4, ((Mesh) parentElement).getToProviderEdges().toArray() );
				Object[] temp6 = ArrayUtils.addAll(temp5, ((Mesh) parentElement).getIntegratorEdges().toArray() );
				return temp6;
			} else if( parentElement instanceof Node) {
				return ((Node) parentElement).getVariables().toArray();
			} else if( parentElement instanceof Edge) {
				return ((Edge) parentElement).getTo().toArray();
			} else if( parentElement instanceof Variable) {
				return ((Variable) parentElement).getAccessors().toArray();
			} else if( parentElement instanceof SimulationRun) {
				Mesh mesh = ((SimulationRun) parentElement).getSequence().getMesh();
				return new Object[] { mesh };
			} else {
				return null;
			}
		}
		@Override
		public Object getParent(Object element) {
			if( element instanceof Accessor) {
				return ((Accessor) element).getVar();
			} else if( element instanceof Variable) {
				return ((Variable) element).getNode();
			} else if( element instanceof Node) {
				return ((Node) element).eContainingFeature();
			} else if( element instanceof Edge) {
				return ((Edge) element).eContainingFeature();
			} else {
				return null;
			}		
		}
		@Override
		public boolean hasChildren(Object element) {
			if( element instanceof Mesh) {
				return (((Mesh) element).getNodes().size() != 0 );
			} else if( element instanceof Node) {
				return (((Node) element).getVariables().size() != 0 );
			} else if( element instanceof Edge) {
				return (((Edge) element).getTo().size() != 0 );
			} else if( element instanceof Variable) {
				return (((Variable) element).getAccessors().size() != 0 );
			} else if( element instanceof SimulationRun) {
				return true;
			} else if( element instanceof Sequence) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	class ViewLabelProvider extends LabelProvider implements ILabelProvider  {
		public String getText(Object obj) {
			if( obj instanceof Accessor) {
				int index = ((Accessor)obj).getIndex();
				return ((Accessor) obj).getVar().get(index).toString();
			} else if( obj instanceof Variable) {
				return ((Variable) obj).getFeature();
			} else if( obj instanceof Node) {
				return ((Node) obj).getName();
			} else if( obj instanceof Edge) {
				return "Edge";
			} else if( obj instanceof Mesh) {
				return "Mesh";
			} else if( obj instanceof SimulationRun) {
				return "Runner";
			} else if( obj instanceof Sequence) {
				return "Sequence";
			} else if( obj instanceof Integrator) {
				return "Integrator";
			} else {
				return "Something else ..." + obj.toString();
			}		
		}
/*		public Image getImage(Object obj) {
			return PlatformUI.getWorkbench().
					getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
		}*/
	}
	
	@Inject
	public PersistencePart(Composite parent) {
		//System.out.println("PersistenceView:PersistenceView");

		PersistencePackageImpl.init();
		PersistenceFactory factory = PersistenceFactory.eINSTANCE;
		Persistence persistence = factory.createPersistence();
			
		@SuppressWarnings("unchecked")
		EList<Mesh> listM = (EList<Mesh>) persistence.getBackend().getStoredMeshes();
		//System.out.println("Meshes loaded: " + listM.toString());
		@SuppressWarnings("unchecked")
		EList<EObject> listR = (EList<EObject>) persistence.getBackend().getStoredRunners();
		//System.out.println("Runners loaded: " + listR.toString());
		
		listR.addAll(listM);
				
		treeViewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		treeViewer.setContentProvider(new ViewContentProvider());
		treeViewer.setLabelProvider(new ViewLabelProvider());
		
		if ( listR != null ) {
			treeViewer.setInput(listR);
		}
		GridLayoutFactory.fillDefaults().generateLayout(parent);
	}
	
	@PostConstruct
	public void postConstruct(Composite parent, EMenuService menuService) {
		//System.out.println("PersistenceView:postConstruct");
		//menuService.registerContextMenu(treeViewer.getControl(),"org.thirdeye.ui.view.persistence.popupmenu.popup");
		menuService.registerContextMenu(parent,"org.thirdeye.ui.view.persistence.popupmenu.popup1");
		menuService.registerContextMenu(parent,"org.thirdeye.ui.view.persistence.popupmenu.popup2");
		
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				selectionService.setSelection(
						selection.size() == 1 ? selection.getFirstElement() : selection.toArray());
			}
		});
	}
}
