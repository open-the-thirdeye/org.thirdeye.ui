/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.ui.view.persistence.handlers;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Named;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.sirius.business.api.dialect.DialectManager;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.eclipse.sirius.tools.api.command.semantic.AddSemanticResourceCommand;
import org.eclipse.sirius.ui.business.api.dialect.DialectUIManager;
import org.eclipse.sirius.ui.business.api.viewpoint.ViewpointSelection;
import org.eclipse.sirius.ui.business.api.viewpoint.ViewpointSelectionCallbackWithConfimation;
import org.eclipse.sirius.ui.business.internal.commands.ChangeViewpointSelectionCommand;
import org.eclipse.sirius.viewpoint.DRepresentation;
import org.eclipse.sirius.viewpoint.description.RepresentationDescription;
import org.eclipse.sirius.viewpoint.description.Viewpoint;
import org.thirdeye.core.persistence.SimulationRun;

@SuppressWarnings("restriction")
public class SiriusHandler {
	
	private Session session;

	private RepresentationDescription getDescription(Session session_p, String descriptionId_p) {
		Collection<Viewpoint> viewpoints = session_p.getSelectedViewpoints(true);
		for (Viewpoint viewpoint : viewpoints) {
			for (RepresentationDescription description : viewpoint.getOwnedRepresentations()) {
				if (descriptionId_p.equals(description.getName())) {
					return description;
				}
			}
		}
		return null;
	}
	
	private static String FILE_EXTENSION = "mesh";
	
	@Execute
	public void execute( @Named(IServiceConstants.ACTIVE_SELECTION) EObject object  ) {
		System.out.println("SiriusHandler:execute");

		if( object instanceof SimulationRun ) {
			object = ((SimulationRun)object).getSequence().getMesh();
		}
		String airdURI = object.eResource().getURI().toString();
		if( airdURI.endsWith(".core")) {
			FILE_EXTENSION = "core";
		} else {
			FILE_EXTENSION = "mesh";
		}

		int position = airdURI.lastIndexOf("/");
		airdURI = (String) airdURI.subSequence(0, position);
		airdURI = airdURI + "/representations.aird";
				
		System.out.println(object.eResource().getURI().toString());
		System.out.println(airdURI.toString());
		
		IProgressMonitor monitor = new NullProgressMonitor();
		
		//create viewpoint
		session = SessionManager.INSTANCE.getSession(URI.createURI(airdURI), monitor);
		
		AddSemanticResourceCommand addCommandToSession = new AddSemanticResourceCommand(session, object.eResource().getURI(), monitor );
		session.getTransactionalEditingDomain().getCommandStack().execute(addCommandToSession);        		

		//find and add viewpoint
		Set<Viewpoint> viewpoints = ViewpointSelection.getViewpoints(FILE_EXTENSION);
		if(viewpoints.isEmpty()) {
		      System.out.println("Could not find viewpoint for file extension " + FILE_EXTENSION);
		}
		                		
		ViewpointSelection.Callback callback = new ViewpointSelectionCallbackWithConfimation();
		                		 
		RecordingCommand command = new ChangeViewpointSelectionCommand(
		                				session,
		                				callback,
		                				viewpoints, new HashSet<Viewpoint>(), monitor);
		                		
		session.getTransactionalEditingDomain().getCommandStack().execute(command);
		
		final DRepresentation myDiagramRepresentation;
		final RepresentationDescription newDescription = getDescription(session, "org.thirdeye.ui.sirius.mesh.diagram");
		final DialectManager viewpointDialectManager = DialectManager.INSTANCE;
		final DialectUIManager dialectUIManager = DialectUIManager.INSTANCE;

		Collection<DRepresentation> representations = viewpointDialectManager.getRepresentations(newDescription, session);
		System.out.println(representations.toString());
		myDiagramRepresentation = representations.iterator().next();
		dialectUIManager.openEditor(session,myDiagramRepresentation, monitor);
		SessionManager.INSTANCE.notifyRepresentationCreated(session);
	}
		
}