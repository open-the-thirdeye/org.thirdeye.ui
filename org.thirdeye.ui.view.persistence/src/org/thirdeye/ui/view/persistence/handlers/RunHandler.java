/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.ui.view.persistence.handlers;


import java.util.Date;

import javax.inject.Named;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.thirdeye.core.mesh.Mesh;
import org.thirdeye.core.persistence.Persistence;
import org.thirdeye.core.persistence.PersistenceFactory;
import org.thirdeye.core.persistence.SimulationRun;
import org.thirdeye.core.persistence.impl.PersistencePackageImpl;
import org.thirdeye.core.sequence.SimpleSequence;

public class RunHandler {
	@Execute
	public void execute( @Named(IServiceConstants.ACTIVE_SELECTION) EObject object  ) {
		//System.out.println("RunHandler:execute object from resource " + object.eResource().toString() + " object: " + object.toString());
		System.out.println("RunHandler:execute starting Simulation");
		String path = object.eResource().toString();
		String id = path.substring(0, path.lastIndexOf('.')) + "_Test";
		PersistencePackageImpl.init();
		PersistenceFactory factory = PersistenceFactory.eINSTANCE;
		Persistence persistence = factory.createPersistence();
		if( object instanceof Mesh ) {
			//core.load((Mesh)object, "mabulla");			
		} else if( object instanceof SimulationRun ) {
			//System.out.println("Run:execute");
			SimulationRun runner = (SimulationRun)object;
			//System.out.println("Setting resource to " + id );
			persistence.getBackend().createLocalResource(id);
			
			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(object);
			if (domain != null) {
				domain.getCommandStack().execute(new RecordingCommand(domain) {
					   public void doExecute() {
						   runner.setStartTime(new Date());
							runner.setSteps(runner.getSteps()/10);
							((SimpleSequence )runner.getSequence()).init();
							runner.setRunning(true);
					   }
					});
			} else {
				runner.setStartTime(new Date());
				runner.setSteps(runner.getSteps()/10);
				((SimpleSequence )runner.getSequence()).init();
				runner.setRunning(true);
			}
			
			Job job = new Job("Runner Job") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					// Set total number of work units
					monitor.beginTask("running", 10);
					System.out.println("Running for " + runner.getSteps()*10 + " steps");
					System.out.println("Start time: " + new Date().toString());
					for (int i = 0; i < 10; i++) {
						if (domain != null) {
							domain.getCommandStack().execute(new RecordingCommand(domain) {
								   public void doExecute() {
										runner.run();
								   }
								});
						} else {
							runner.run();
						}
						monitor.subTask("doing " + i);
						monitor.worked(1);
					}
					System.out.println("Simulation finished: " + new Date().toString());
					persistence.doSave();
					System.out.println("Persistence finished: " + new Date().toString());
					runner.setSteps(runner.getSteps()*10);
					runner.setRunning(false);
					return Status.OK_STATUS;
				}
			};
			job.schedule();
		} 
	}	
}