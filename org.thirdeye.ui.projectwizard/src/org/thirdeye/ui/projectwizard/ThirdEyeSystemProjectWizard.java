/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.ui.projectwizard;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;

public class ThirdEyeSystemProjectWizard extends Wizard implements INewWizard {

	private ThirdEyeSystemProjectWizardPage page;

	private ISelection selection;

	private IConfigurationElement configElement;

	public ThirdEyeSystemProjectWizard() {
		super();
		setNeedsProgressMonitor(true);
	}

	@Override
	public void addPages() {
		page = new ThirdEyeSystemProjectWizardPage(selection);
		addPage(page);
	}

	@Override
	public boolean performFinish() {
		final String name = page.getProjectName();
		final boolean genExample = page.isCreateExample();
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(name, genExample, monitor);
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		BasicNewProjectResourceWizard.updatePerspective(configElement);
		return true;
	}

	void doFinish(String name, boolean genExample, IProgressMonitor monitor) {
		String projectName = name;
		monitor.beginTask("Creating ThirdEye System project " + name, 2);

		Set<String> refs = new HashSet<String>();
		refs.add("org.eclipse.core.runtime");
		refs.add("org.eclipse.emf.ecore");
		
		List<String> srcfolders = new ArrayList<String>();
		srcfolders.add("src");
		srcfolders.add("src-gen");

		IProject p = ThirdEyeProjectHelper.createProject(projectName, srcfolders, Collections.<IProject> emptyList(), refs,
				null, monitor, getShell());

		if (p == null) {
			return;
		}
		if (genExample) {
			ThirdEyeProjectHelper.createFile("model/Example.mesh", p, getContents("Example.mesh"), monitor);
			ThirdEyeProjectHelper.createFile("persistence/Example.persistence", p, getContents("Example.persistence"), monitor);
			//ThirdEyeProjectHelper.createFile("persistence/2.persistence", p, getContents("2.persistence"), monitor);
			//ThirdEyeProjectHelper.createFile("persistence/3.persistence", p, getContents("3.persistence"), monitor);
			//ThirdEyeProjectHelper.createFile("persistence/4.persistence", p, getContents("4.persistence"), monitor);
			//ThirdEyeProjectHelper.createFile("persistence/5.persistence", p, getContents("5.persistence"), monitor);
		}
		monitor.worked(1);
	}

	private String getContents(String resource) {
		try {
			
			URL url = new URL("platform:/plugin/org.thirdeye.ui.projectwizard/example/" + resource);
			InputStream inputStream = url.openConnection().getInputStream();
			
			byte[] buffer = new byte[4096];
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

			while (true) {
				int read;
				read = inputStream.read(buffer);

				if (read == -1) {
					break;
				}

				outputStream.write(buffer, 0, read);
			}

			outputStream.close();
			inputStream.close();

			return outputStream.toString("iso-8859-1");
		} catch (IOException e) {
			return "";
		}
	}

	/**
	 * We will accept the selection in the workbench to see if we can initialize
	 * from it.
	 * 
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}

	public void setInitializationData(IConfigurationElement config, String propertyName, Object data)
			throws CoreException {
		this.configElement = config;
	}
}
