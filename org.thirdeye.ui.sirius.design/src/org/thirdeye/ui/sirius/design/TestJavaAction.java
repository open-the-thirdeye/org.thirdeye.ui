/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.ui.sirius.design;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.sirius.tools.api.ui.IExternalJavaAction;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;
import org.thirdeye.core.mesh.Connection;
import org.thirdeye.core.mesh.Mesh;
import org.thirdeye.core.mesh.Model;
import org.thirdeye.core.mesh.Node;

public class TestJavaAction implements IExternalJavaAction {

	public TestJavaAction() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean canExecute(Collection<? extends EObject> arg0) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void execute(Collection<? extends EObject> arg0, Map<String, Object> arg1) {

		List<String> list = new ArrayList<String>();
		Registry registry = EPackage.Registry.INSTANCE;
		for (String key : new HashSet<String>(registry.keySet())) {
		    EPackage ePackage = registry.getEPackage(key);
		    if (ePackage.getClass().getName().startsWith("org.thirdeye.extension")) {
				//System.out.println(ePackage.getClass().getName());
		    	for(EClassifier eClass: ePackage.getEClassifiers() ) {
					//System.out.println("	" + eClass.getName());
					list.add(eClass.getName());
		    	}
		    }
		}

		ElementListSelectionDialog dialog =
		        new ElementListSelectionDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), new LabelProvider());
		dialog.setElements(list.toArray());
		dialog.setTitle("Choose Model to create");
		dialog.open();
		Object[] result = dialog.getResult();

		//System.out.println(result[0].toString());
		
		EPackage spacePackage = registry.getEPackage("http://space/1.0");
    	EClass spaceClass = (EClass)spacePackage.getEClassifier(result[0].toString());
		EPackage wwPackage = registry.getEPackage("http://worldwind/1.0");
    	EClass wwClass = (EClass)wwPackage.getEClassifier(result[0].toString());
		EPackage examplePackage = registry.getEPackage("http://example/1.0");
    	EClass exampleClass = (EClass)examplePackage.getEClassifier(result[0].toString());
		EPackage sunPackage = registry.getEPackage("http://sun/1.0");
    	EClass sunClass = (EClass)sunPackage.getEClassifier(result[0].toString());
    	EObject obj;
    	if( spaceClass != null ) {
    		obj = EcoreUtil.create(spaceClass);    		
    	} else if( wwClass != null ) {
    		obj = EcoreUtil.create(wwClass);    		
    	} else if( exampleClass != null ) {
    		obj = EcoreUtil.create(exampleClass);    		
    	} else {
    		obj = EcoreUtil.create(sunClass);    		
    	}
		
		//System.out.println("	" + obj.toString());

		EObject temp = (EObject) arg0.toArray()[0];
		Mesh mesh = (Mesh)temp;
		if( obj instanceof Model ) {
			mesh.getModelNodes().add((Node)obj);			
		} else if( obj instanceof Connection ) {
			mesh.getConnectionNodes().add((Node)obj);			
		} else {
			mesh.getProviderNodes().add((Node)obj);			
		}  
	}

}
