/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.ui.sirius.design;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.factory.SessionFactory;

public class MongoSessionFactory implements SessionFactory {

	public MongoSessionFactory() {
		System.out.println("MongoSessionFactory:MongoSessionFactory");
	}

	@Override
	public Session createDefaultSession(URI arg0) throws CoreException {
		System.out.println("MongoSessionFactory:createDefaultSession");
		return null;
	}

	@Override
	public Session createSession(URI arg0, IProgressMonitor arg1) throws CoreException {
		System.out.println("MongoSessionFactory:createSession");
		return null;
	}

}
