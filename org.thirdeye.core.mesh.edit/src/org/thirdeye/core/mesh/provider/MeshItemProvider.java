/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.mesh.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.thirdeye.core.mesh.Mesh;
import org.thirdeye.core.mesh.MeshFactory;
import org.thirdeye.core.mesh.MeshPackage;

/**
 * This is the item provider adapter for a {@link org.thirdeye.core.mesh.Mesh} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MeshItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MeshItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
//			childrenFeatures.add(MeshPackage.Literals.MESH__EDGES);
//			childrenFeatures.add(MeshPackage.Literals.MESH__NODES);
			childrenFeatures.add(MeshPackage.Literals.MESH__MODEL_NODES);
			childrenFeatures.add(MeshPackage.Literals.MESH__CONNECTION_NODES);
			childrenFeatures.add(MeshPackage.Literals.MESH__PROVIDER_NODES);
			childrenFeatures.add(MeshPackage.Literals.MESH__FROM_MODEL_EDGES);
			childrenFeatures.add(MeshPackage.Literals.MESH__FROM_CONNECTION_EDGES);
			childrenFeatures.add(MeshPackage.Literals.MESH__TO_PROVIDER_EDGES);
			childrenFeatures.add(MeshPackage.Literals.MESH__INTEGRATOR_EDGES);

		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Mesh.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Mesh"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_Mesh_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Mesh.class)) {
			case MeshPackage.MESH__EDGES:
			case MeshPackage.MESH__NODES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(MeshPackage.Literals.MESH__EDGES,
				 FeatureMapUtil.createEntry
					(MeshPackage.Literals.MESH__INTEGRATOR_EDGES,
					 MeshFactory.eINSTANCE.createEdge())));

		newChildDescriptors.add
			(createChildParameter
				(MeshPackage.Literals.MESH__EDGES,
				 FeatureMapUtil.createEntry
					(MeshPackage.Literals.MESH__INTEGRATOR_EDGES,
					 MeshFactory.eINSTANCE.createTransformationEdge())));

		newChildDescriptors.add
			(createChildParameter
				(MeshPackage.Literals.MESH__EDGES,
				 FeatureMapUtil.createEntry
					(MeshPackage.Literals.MESH__FROM_MODEL_EDGES,
					 MeshFactory.eINSTANCE.createEdge())));

		newChildDescriptors.add
			(createChildParameter
				(MeshPackage.Literals.MESH__EDGES,
				 FeatureMapUtil.createEntry
					(MeshPackage.Literals.MESH__FROM_MODEL_EDGES,
					 MeshFactory.eINSTANCE.createTransformationEdge())));

		newChildDescriptors.add
			(createChildParameter
				(MeshPackage.Literals.MESH__EDGES,
				 FeatureMapUtil.createEntry
					(MeshPackage.Literals.MESH__TO_PROVIDER_EDGES,
					 MeshFactory.eINSTANCE.createEdge())));

		newChildDescriptors.add
			(createChildParameter
				(MeshPackage.Literals.MESH__EDGES,
				 FeatureMapUtil.createEntry
					(MeshPackage.Literals.MESH__TO_PROVIDER_EDGES,
					 MeshFactory.eINSTANCE.createTransformationEdge())));

		newChildDescriptors.add
			(createChildParameter
				(MeshPackage.Literals.MESH__EDGES,
				 FeatureMapUtil.createEntry
					(MeshPackage.Literals.MESH__FROM_CONNECTION_EDGES,
					 MeshFactory.eINSTANCE.createEdge())));

		newChildDescriptors.add
			(createChildParameter
				(MeshPackage.Literals.MESH__EDGES,
				 FeatureMapUtil.createEntry
					(MeshPackage.Literals.MESH__FROM_CONNECTION_EDGES,
					 MeshFactory.eINSTANCE.createTransformationEdge())));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		if (childFeature instanceof EStructuralFeature && FeatureMapUtil.isFeatureMap((EStructuralFeature)childFeature)) {
			FeatureMap.Entry entry = (FeatureMap.Entry)childObject;
			childFeature = entry.getEStructuralFeature();
			childObject = entry.getValue();
		}

		boolean qualify =
			childFeature == MeshPackage.Literals.MESH__INTEGRATOR_EDGES ||
			childFeature == MeshPackage.Literals.MESH__FROM_MODEL_EDGES ||
			childFeature == MeshPackage.Literals.MESH__TO_PROVIDER_EDGES ||
			childFeature == MeshPackage.Literals.MESH__FROM_CONNECTION_EDGES;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MeshEditPlugin.INSTANCE;
	}

}
