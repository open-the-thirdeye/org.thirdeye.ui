/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.ui.view.plot3d;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;


import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.swt.widgets.Composite;
import org.jzy3d.bridge.swt.Bridge;
import org.jzy3d.chart.Chart;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.thirdeye.core.mesh.Variable;
import org.thirdeye.core.persistence.PersistedNode;
import org.thirdeye.core.persistence.Persistence;
import org.thirdeye.core.persistence.PersistenceFactory;
import org.thirdeye.core.persistence.SimulationRun;
import org.thirdeye.core.persistence.impl.PersistencePackageImpl;

public class Plot3dPart {

	public static final String ID = "sc.ndt.aml.view.ScatterPlot3DView"; //$NON-NLS-1$
	private Chart chart;
	private Composite parent;
	
	@Inject
	public Plot3dPart(Composite par) {
		//System.out.println("Plot3dPart::Plot3dPart");

		parent = par;

	}
	
	@Inject
	public void setSelection(
	 @Named(IServiceConstants.ACTIVE_SELECTION)@Optional Variable var ){
		//System.out.println("Plot3dPart::setSelection");

		if(var != null) {
			SimulationRun run = (SimulationRun)var.eContainer().eContainer().eContainer().eContainer();
			chart = getChart(var, run.getDescription());
			Bridge.adapt(parent, (Component) chart.getCanvas());
		}

	 }

	
	public Chart getChart(Variable var, String runnerDescription ) {
		//System.out.println("Plot3dPart::getChart");

        PersistencePackageImpl.init();
        PersistenceFactory factory = PersistenceFactory.eINSTANCE;
        Persistence persistence = factory.createPersistence();

		@SuppressWarnings("unchecked")
		EList<EObject> list = (EList<EObject>) persistence.getBackend().getStoredNodes(var,runnerDescription);
		List<Object> tempList = new ArrayList<Object>();
		
		for( EObject node: list ) {
			if( node instanceof PersistedNode ) {
				EStructuralFeature tempFeature = ((PersistedNode)node).getNode().eClass().getEStructuralFeature(var.getFeature());
				if(tempFeature!=null) {
					Object o = ((PersistedNode)node).getNode().eGet(tempFeature);
					tempList.add( o );
				}
			}
		}
			
		Object[] tempArray = tempList.toArray();
		int size = tempArray.length;
        
        float x, y, z;
		//System.out.println( "Found " + size + " entries with name " + var.getNode().getName() );

        Coord3d[] points = new Coord3d[size];
        Color[] colors = new Color[size];
        
        for(int i=0; i<size; i++){
        	EDataTypeEList<?> temp = (EDataTypeEList<?>) tempArray[i];
            x = ((Double)temp.get(0)).floatValue();
            y = ((Double)temp.get(1)).floatValue();
            z = ((Double)temp.get(2)).floatValue();
            //System.out.println( "X: " + x + " Y: " + y + " Z: " + z );
            points[i] = new Coord3d(x, y, z);
            colors[i] = Color.BLACK;
        }
        
        Scatter scatter = new Scatter(points, colors);
        scatter.setWidth(5.0f);
        chart = AWTChartComponentFactory.chart(Quality.Advanced, "awt");
        chart.getScene().add(scatter);
        chart.addMouseController();
        
		return chart;
		
        /*int size = 500000;
        float x;
        float y;
        float z;
        float a;
        
        Coord3d[] points = new Coord3d[size];
        Color[] colors = new Color[size];
        
        Random r = new Random();
        r.setSeed(0);
        
        for(int i=0; i<size; i++){
            x = r.nextFloat() - 0.5f;
            y = r.nextFloat() - 0.5f;
            z = r.nextFloat() - 0.5f;
            points[i] = new Coord3d(x, y, z);
            a = 0.25f;
            colors[i] = new Color(x, y, z, a);
        }
        
        Scatter scatter = new Scatter(points, colors);
        chart = AWTChartComponentFactory.chart(Quality.Advanced, "awt");
        chart.getScene().add(scatter);
        chart.addMouseController();

		return chart;*/
	}
}