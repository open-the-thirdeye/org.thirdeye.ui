/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.ui.view.plot;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.experimental.chart.swt.ChartComposite;
import org.thirdeye.core.mesh.Variable;
import org.thirdeye.core.persistence.PersistedNode;
import org.thirdeye.core.persistence.Persistence;
import org.thirdeye.core.persistence.PersistenceFactory;
import org.thirdeye.core.persistence.SimulationRun;
import org.thirdeye.core.persistence.impl.PersistencePackageImpl;

public class PlotPart {
	
	private JFreeChart chart;
	private Composite parent;
	private ChartComposite cc;

	@Inject
	public PlotPart(Composite par) {
		//System.out.println("JFreechartView::JFreechartView");

		parent = par;
	}
	
	@Inject
	public void setSelection(
	 @Named(IServiceConstants.ACTIVE_SELECTION)@Optional Variable var ){
		//System.out.println("JFreechartView::setSelection");

		if(var != null) {
			//System.out.println("Var:" + var.getFeature() + " " + var.getNode().getName());
			SimulationRun run = (SimulationRun)var.eContainer().eContainer().eContainer().eContainer();
			chart = getChart(var, run);
			if(cc==null) {
				cc = new ChartComposite(parent, SWT.NONE, chart, true );	
			} else {
				cc.dispose();
				cc = new ChartComposite(parent, SWT.NONE, chart, true );	
			}
			cc.forceRedraw();
		}
	 }
	
	public JFreeChart getChart(Variable var, SimulationRun run ) {
		//System.out.println("JFreechartView::getChart");

        final XYSeriesCollection set = new XYSeriesCollection();

        PersistencePackageImpl.init();
        PersistenceFactory factory = PersistenceFactory.eINSTANCE;
        Persistence persistence = factory.createPersistence();

		@SuppressWarnings("unchecked")
		EList<EObject> list = (EList<EObject>) persistence.getBackend().getStoredNodes(var,run.eResource().toString());
		List<Object> tempList = new ArrayList<Object>();
		//System.out.println(list.size() + " models found");
		for( EObject node: list ) {
			if( node instanceof PersistedNode ) {
				//System.out.println("Node: " + node.toString());
				EStructuralFeature tempFeature = ((PersistedNode)node).getNode().eClass().getEStructuralFeature(var.getFeature());
				if(tempFeature!=null) {
					Object o = ((PersistedNode)node).getNode().eGet(tempFeature);
					tempList.add( o );
				}
			}
		}
		
		Object[] tempArray = tempList.toArray();
		if( tempArray.length != 0 ) {
			if( tempArray[0] instanceof EList ) {
				EList<?> temp = (EList<?>)tempArray[0];
				final XYSeries[] series = new XYSeries[temp.size()];
				for(int i=0; i<temp.size(); i++){
					series[i] = new XYSeries(var.getFeature() + "[" + i + "]");
				}
		        for(int i=0; i<tempArray.length; i++){
		        	@SuppressWarnings("rawtypes")
					EList newList = (EList<?>)tempArray[i];
		        	for(int j=0; j<newList.size(); j++) {
			        	series[j].add(i,(double) newList.get(j));
		        	}
		        }
				for(int i=0; i<temp.size(); i++){
					set.addSeries(series[i]);
				}
			} else {
		        final XYSeries series = new XYSeries(var.getFeature());
		        boolean isInt = false;
		        if(tempArray[0] instanceof Integer ) {
		        	isInt = true;
		        }
		        for(int i=0; i<tempArray.length; i++){
		        	if( isInt ) {
			        	series.add(i,(int)tempArray[i]);
		        	} else {
			        	series.add(i,(double) tempArray[i]);
		        	}
		        }
		        set.addSeries(series);			
			}
			return ChartFactory.createXYLineChart(var.getFeature(), "step", var.getUnit(), set, PlotOrientation.VERTICAL,true,true,false);
		} else {
			return ChartFactory.createXYLineChart("none", "step", "none", set, PlotOrientation.VERTICAL,true,true,false);			
		}
	}
}