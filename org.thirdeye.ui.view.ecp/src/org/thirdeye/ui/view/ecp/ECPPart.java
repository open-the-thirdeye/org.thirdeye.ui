/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.ui.view.ecp;

import javax.inject.Inject;
import javax.inject.Named;
import javax.annotation.PostConstruct;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecp.ui.view.ECPRendererException;
import org.eclipse.emf.ecp.ui.view.swt.DefaultReferenceService;
import org.eclipse.emf.ecp.ui.view.swt.ECPSWTView;
import org.eclipse.emf.ecp.ui.view.swt.ECPSWTViewRenderer;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContextFactory;
import org.eclipse.emf.ecp.view.spi.model.ModelChangeListener;
import org.eclipse.emf.ecp.view.spi.model.ModelChangeNotification;
import org.eclipse.emf.ecp.view.spi.provider.ViewProviderHelper;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.thirdeye.core.mesh.Mesh;
import org.thirdeye.core.mesh.Node;
import org.thirdeye.core.mesh.Variable;
import org.thirdeye.core.persistence.Persistence;
import org.thirdeye.core.persistence.PersistenceFactory;
import org.thirdeye.core.persistence.SimulationRun;
import org.thirdeye.core.persistence.impl.PersistencePackageImpl;

public class ECPPart {
	
	private static EObject obj = null;
	private ViewModelContext vmc;
	private Composite content;
	private Composite parent;
	private ECPSWTView view = null;
	
	@Inject
	public ECPPart(Composite par) {
		//System.out.println("ECPPart::ECPPart");

		parent = par;
	}

	@Inject
	public void setSelection(
	 @Named(IServiceConstants.ACTIVE_SELECTION)@Optional EObject object ){
		//System.out.println("ECPPart::setSelection");

		if(object != null) {
			//System.out.println("Object: " + object.toString());
			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(object);
			if (domain != null) {
				domain.getCommandStack().execute(new RecordingCommand(domain) {
					   public void doExecute() {
							if( object instanceof Mesh ) {
								Mesh mesh = (Mesh)object;
								for( Node node: mesh.getModelNodes() ) {
									for( Variable var: node.getVariables() ) var.getAccessors();
								}
								for( Node node: mesh.getConnectionNodes() ) {
									for( Variable var: node.getVariables() ) var.getAccessors();
								}
								for( Node node: mesh.getProviderNodes() ) {
									for( Variable var: node.getVariables() ) var.getAccessors();
								}
							} else if( object instanceof SimulationRun ) {
								//SimpleSequence seq = (SimpleSequence)((SimulationRun)object).getSequence();
								//seq.init();
							}
					   }
				});
			} else {
				if( object instanceof Mesh ) {
					Mesh mesh = (Mesh)object;
					for( Node node: mesh.getModelNodes() ) {
						for( Variable var: node.getVariables() ) var.getAccessors();
					}
					for( Node node: mesh.getConnectionNodes() ) {
						for( Variable var: node.getVariables() ) var.getAccessors();
					}
					for( Node node: mesh.getProviderNodes() ) {
						for( Variable var: node.getVariables() ) var.getAccessors();
					}
				} else if( object instanceof SimulationRun ) {
					//SimpleSequence seq = (SimpleSequence)((SimulationRun)object).getSequence();
					//seq.init();
				}
			}	   

			obj = object;
			if( view != null ) {
				view.dispose();
			}
			if( content != null ) {
				content.dispose();
			}
			if( vmc != null ) {
				vmc.dispose();
			}
			content = new Composite(parent, SWT.NONE);
			content.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
			content.setLayout(GridLayoutFactory.fillDefaults().margins(10, 10).create());
			content.setLayoutData(GridDataFactory.fillDefaults().create());
			
			if (domain != null) {
				domain.getCommandStack().execute(new RecordingCommand(domain) {
					   public void doExecute() {
						   vmc = ViewModelContextFactory.INSTANCE.createViewModelContext(
									ViewProviderHelper.getView(obj, null), obj, new DefaultReferenceService());
					   }
					});
			} else {
				vmc = ViewModelContextFactory.INSTANCE.createViewModelContext(
						ViewProviderHelper.getView(obj, null), obj, new DefaultReferenceService());				
			}
			
			vmc.registerDomainChangeListener(new ModelChangeListener() {
				public void notifyChange(ModelChangeNotification notification) {
					// TODO Auto-generated method stub
					//System.out.println("ModelChangeListener:notifyChange");
				}
			});
			try {
				view = ECPSWTViewRenderer.INSTANCE.render(content, vmc);
			} catch (ECPRendererException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			content.layout();
			parent.layout();
		}
	 }

	@PostConstruct
	public void postConstruct(Composite _parent, EMenuService menuService) {
		//System.out.println("ECPPart::postConstruct");
		menuService.registerContextMenu(_parent,"org.thirdeye.ui.view.ecp.popupmenu.popup");

		parent = _parent;
		PersistencePackageImpl.init();
		PersistenceFactory factory = PersistenceFactory.eINSTANCE;
		Persistence persistence = factory.createPersistence();
		
		@SuppressWarnings("unchecked")
		EList<Mesh> list = (EList<Mesh>) persistence.getBackend().getStoredMeshes();

		for( Mesh mesh: list ) {
			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(mesh);
			if (domain != null) {
				domain.getCommandStack().execute(new RecordingCommand(domain) {
					   public void doExecute() {
							for( Node node: mesh.getModelNodes() ) {
								for( Variable var: node.getVariables() ) var.getAccessors();
							}
							for( Node node: mesh.getConnectionNodes() ) {
								for( Variable var: node.getVariables() ) var.getAccessors();
							}
							for( Node node: mesh.getProviderNodes() ) {
								for( Variable var: node.getVariables() ) var.getAccessors();
							}
					   }
					});
			} else {
				for( Node node: mesh.getModelNodes() ) {
					for( Variable var: node.getVariables() ) var.getAccessors();
				}
				for( Node node: mesh.getConnectionNodes() ) {
					for( Variable var: node.getVariables() ) var.getAccessors();
				}
				for( Node node: mesh.getProviderNodes() ) {
					for( Variable var: node.getVariables() ) var.getAccessors();
				}
			}
		}

		if( list.size() != 0 ) {
			obj = list.get(0);
			
			if( view != null ) {
				view.dispose();
			}
			if( content != null ) {
				content.dispose();
			}
			if( vmc != null ) {
				vmc.dispose();
			}
			
			content = new Composite(parent, SWT.NONE);
			content.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
			content.setLayout(GridLayoutFactory.fillDefaults().margins(10, 10).create());
			content.setLayoutData(GridDataFactory.fillDefaults().create());
			
			vmc = ViewModelContextFactory.INSTANCE.createViewModelContext(
					ViewProviderHelper.getView(obj, null), obj, new DefaultReferenceService());
		 	vmc.registerDomainChangeListener(new ModelChangeListener() {
				public void notifyChange(ModelChangeNotification notification) {
					// TODO Auto-generated method stub
					//System.out.println("ModelChangeListener:notifyChange");
				}
			});
		 	try {
				view = ECPSWTViewRenderer.INSTANCE.render(content, vmc);
			} catch (ECPRendererException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			content.layout();
		}
		parent.layout();
	}
	
	//public static EObject getObject() {
	//	return obj;
	//}
}