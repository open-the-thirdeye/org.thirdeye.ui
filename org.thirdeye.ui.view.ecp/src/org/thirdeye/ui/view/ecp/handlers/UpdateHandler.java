/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.ui.view.ecp.handlers;

import org.eclipse.e4.core.di.annotations.Execute;

public class UpdateHandler {
	@Execute
	public void execute() {
		System.out.println("UpdateHandler:execute");
		//Mesh temp = ((Mesh)ECPPart.getObject());
/*		try {
			MongoComponent.saveEObjectToMongo(temp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
		
}